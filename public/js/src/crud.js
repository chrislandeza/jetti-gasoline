
/* CSRF token for our ajax request */
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

/**
 * Datatable Initialization
 */
var dT = $('#app-datatable').DataTable({
    "aaSorting": [[0, "asc"]],
    "deferRender": true
});


/**
 * Custom directive.
 * 
 * Watch the change event on the datepicker 
 * and update the input.model
 */
Vue.directive('datetimepicker', {
    twoWay: true, // note the two-way binding
    bind: function () {
        $(this.el).change(function(ev) {
                // two-way set
                this.set(this.el.value);
        }.bind(this));
    }
});


/* Create a new instance of vue */
var vm = new Vue({
    el: '#app',
    data: {
        inputs: {
            _token: CSRF_TOKEN
        },
        processingRequest: false,
        form: {
            title: '',
            errors: {}
        },
        rowToProcess: ''
    },
    computed: {
        
        /**
         * Checks if there's an error in the form.
         * 
         * @returns {Boolean}
         */
        errors: function () {
            for (var key in this.form.errors) {
                if (this.form.errors[key])
                    return true;
            }
        }
    },
    methods: {
        
        /**
         * Process the form for creating a new resource
         * 
         * @param {str} formTitle
         * @param {obj} e - The Original DOM Event
         * @returns {void}
         */
        create: function (formTitle, e) {

            /* Reset the form */
            this.resetForm();

            /* Set the form title */
            this.form.title = formTitle;

            /* Get the Form */
            this.getForm(e.target.getAttribute('data-target'));

        },
        
        /**
         * Process the newly created resource
         * 
         * @param {obj} e - The Original DOM Event
         * @returns {void}
         */
        store: function (e) {
            e.preventDefault();
            
            this.syncResource('POST', e.target.action);
        },
        
        /**
         * Process the form for editing a resource
         * 
         * @param {str} formTitle
         * @param {obj} e - The Original DOM Event
         * @returns {void}
         */
        edit: function (formTitle, e) {

            /* Reset the form First */
            this.resetForm();
            
            /* Set the form title */
            this.form.title = formTitle;

            /* The row that we will process */
            this.rowToProcess = $(e.target).closest('tr');

            /* Get the Form */
            this.getForm(e.target.getAttribute('data-target'));

        },
        
        /**
         * Process the edited  resource.
         * 
         * @param {obj} e - The Original DOM Event
         * @returns {void}
         */
        update: function (e) {
            
            e.preventDefault();
            
            this.syncResource('PATCH', e.target.action);
        },
        
        /**
         * Handles the Deletion of Resource
         * 
         * @param {type} e
         * @returns {void}
         */
        destroy: function (e) {

            /* the row that will be deleted */
            this.rowToProcess = $(e.target).closest('tr');
            
            var self = this;
            
            /* Confirm the deletion */
            bootbox.confirm("Do you really want to Delete?", function (result) {
                if (result) {
                    self.syncResource('DELETE', e.target.getAttribute('data-target'));
                }

            });

        },
        
        /**
         * Sync the Resource depending on the given type (method)
         * 
         * @param {str} type - The request method
         * @param {str} url - The url or action
         * @returns {void}
         */
        syncResource: function (type, url) {
            
            this.processingRequest = true;

            var self = this;
            
            $.ajax({
                url: url,
                type: type,
                data: self.inputs,
                dataType: 'JSON',
                success: function (data) {
                    self.processFormSuccess(data, type);
                },
                error: function (data) {
                    self.processFormErrors(data);
                }
            });

        },
        
        /**
         * Get the given URL Form.
         * 
         * @param {str} url
         * @returns {void}
         */
        getForm: function (url) {

            /* Disable the Buttons */
            this.processingRequest = true;

            var self = this;

            /* Start the ajax request */
            $.ajax({
                url: url,
                type: 'GET',
                data: {_token: CSRF_TOKEN},
                dataType: 'JSON',
                success: function (data) {

                    /* Change the form-content and store it to element variable */
                    var $element = $('#form-content').html(data.html);

                    /* compile the new content so that vue can read it */
                    self.$compile($element.get(0));
                    
                    /* Re-initialize all plugin for new Content */
                    initAllPlugin();
                    
                    /* show the form */
                    self.showForm();

                },
                error: function (data) {
                    self.processFormErrors(data);
                }
            });

        },
        
        /**
         * Show the Form Modal
         * 
         * @returns {void}
         */
        showForm: function () {

            /* Enable the buttons */
            this.processingRequest = false;

            /* Show the form modal */
            $('#form-modal').modal('show');

        },
        
        /**
         * Process the Form Success
         * 
         * @param {json} data - the remote data.
         * @param {type} type - The request method
         * @returns {void}
         */
        processFormSuccess: function (data, type) {

            /* Update the datatable. */
            this.updateDataTable(data, type);

            /**
             * Recompile the users-table so that the  
             * updated row can be read by vue.js
             */
            var $element = $('#app-datatable');
            this.$compile($element.get(0));

            // Reset the form
            this.resetForm();

            /* Close the modal */
            $('#form-modal').modal('hide');

            /* Notify the user */
            $.notify('The database has been successfully updated', {
                autoHide: true,
                globalPosition: 'bottom right',
                className: 'success',
                autoHideDelay: 4000
            });

            /* Enable the buttons */
            this.processingRequest = false;

        },
        
        /*
         * Process the Form error
         * 
         * @param {json} data - the remote data.
         * @returns {void}
         */
        processFormErrors: function (data) {

            var self = this;

            /* Clear the errors array */
            self.form.errors = [];

            /**
             * if the error code is 422 then parse it to json, otherwise, 
             * just throw an ajax error notification
             */
            if (data.status === 422) {
                self.form.errors = $.parseJSON(data.responseText);
            } else {
                throwAjaxError();
            }

            /* Enable the buttons */
            self.processingRequest = false;

        },
        
        /**
         * Update the Client dataTable
         * 
         * @param {json} data - the remote data.
         * @param {type} type - The request method
         * @returns {void}
         */
        updateDataTable: function (data, type) {

            switch (type) {
                case 'PATCH':
                    
                    /* Get the row */
                    var row = dT.row(this.rowToProcess);

                    /* Update */
                    row.data(data.row).draw();

                    break;

                case 'DELETE':
                    
                    /* Get the row to be removed */
                    var row = dT.row(this.rowToProcess);

                    /* Remove the user from the Datatable */
                    row.remove().draw(false);

                    break;

                case 'POST':

                    /* Add the new row on the users datatable */
                    dT.row.add(data.row).draw();

                    break;
            }
        },
        
        /**
         * Clear all the inputs property except the _token
         * 
         * @returns {void}
         */
        resetForm: function () {

            this.rowToProcess = '';

            this.form.errors = [];

            /* Clear the fields except the token. */
            for (var key in this.inputs) {
                if (key != '_token') {
                    this.inputs[key] = '';
                }
            }
        }
    }
});

