var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

/**
 * Datatable Initialization
 */
var productCategoriesTable = $('#product-categories-table').DataTable({
    "aaSorting": [[0, "asc"]]
});

var vm = new Vue({
    el: '#app',
    data: {
        inputs: {
            id: '',
            category_name: '',
            category_description: '',
            password_confirmation: '',
            _token: CSRF_TOKEN
        },
        processingRequest: false,
        form: {
            title: '',
            errors: {}
        },
        rowToProcess: ''

    },
    computed: {
        errors: function () {
            for (var key in this.form.errors) {
                if (this.form.errors[key])
                    return true;
            }
        }
    },
    methods: {
        createCategory: function (e) {

            e.preventDefault();

            /* Reset the form */
            this.resetForm();

            /* Set the form title */
            this.form.title = 'New Category';

            /* Get the Form */
            this.getForm(e.target.getAttribute('data-target'));


        },
        addCategory: function (e) {

            e.preventDefault();

            this.processingRequest = true;

            var self = this;

            $.ajax({
                url: e.target.action,
                type: 'POST',
                data: self.inputs,
                dataType: 'JSON',
                success: function (data) {
                    self.processFormSuccess(data);
                },
                error: function (data) {
                    self.processFormErrors(data);
                }
            });


        },
        editCategory: function (e) {

            /* Reset the form First */
            this.resetForm();

            /* The row that we will process */
            this.rowToProcess = $(e.target).closest('tr');

            /* Set the form title */
            this.form.title = 'Update Category';

            /* Get the Form */
            this.getForm(e.target.getAttribute('data-target'));

        },
        updateCategory: function (e) {

            e.preventDefault();

            this.processingRequest = true;

            var self = this;

            $.ajax({
                url: e.target.action,
                type: 'PATCH',
                data: self.inputs,
                dataType: 'JSON',
                success: function (data) {
                    self.processFormSuccess(data, 'edit');
                },
                error: function (data) {
                    self.processFormErrors(data);
                }
            });

        },
        deleteCategory: function (e) {

            e.preventDefault();

            /* the row that will be deleted */
            this.rowToProcess = $(e.target).closest('tr');

            var url = e.target.href;

            var self = this;

            bootbox.confirm("Do you really want to Delete?", function (result) {
                if (result) {
                    $.ajax({
                        url: url,
                        type: 'DELETE',
                        data: {_token: CSRF_TOKEN},
                        dataType: 'JSON',
                        error: function (data) {
                            self.processFormErrors(data);
                        },
                        success: function (data) {
                            self.processFormSuccess(data, 'delete');
                        }
                    });
                }

            });


        },
        getForm: function (url) {

            /* Disable the Buttons */
            this.processingRequest = true;

            var self = this;

            /* Start the ajax request */
            $.ajax({
                url: url,
                type: 'GET',
                data: {_token: CSRF_TOKEN},
                dataType: 'JSON',
                success: function (data) {

                    /* Change the form-content and store it to element variable */
                    var $element = $('#form-content').html(data.html);

                    /* compile the new content so that vue can read it */
                    self.$compile($element.get(0));

                    /* show the form */
                    self.showForm();

                },
                error: function (data) {
                    self.processFormErrors(data);
                }
            });

        },
        showForm: function () {

            /* Enable the buttons */
            this.processingRequest = false;

            /* Show the form modal */
            $('#category-form-modal').modal('show');


        },
        processFormSuccess: function (data, type) {

            /* Update the datatable. */
            this.updateDataTable(data, type);

            /**
             * Recompile the users-table so that the  
             * updated row can be read by vue.js
             */
            var $element = $('#product-categories-table');
            this.$compile($element.get(0));


            // Reset the form
            this.resetForm();

            /* Close the modal */
            $('#category-form-modal').modal('hide');

            /* Notify the user */
            $.notify('The database has been successfully updated', {
                autoHide: true,
                globalPosition: 'bottom right',
                className: 'success',
                autoHideDelay: 4000
            });

            /* Enable the buttons */
            this.processingRequest = false;

        },
        processFormErrors: function (data) {

            var self = this;

            /* Clear the errors array */
            self.form.errors = [];

            /**
             * If the error status is 422 then parse it to JSON
             * and store it to our form.errors data. otherwise
             * just throw an error notification
             */
            if (data.status === 422) {
                self.form.errors = $.parseJSON(data.responseText);
            } else {
                throwAjaxError();
            }
            /* Enable the buttons */
            self.processingRequest = false;

        },
        updateDataTable: function (data, type) {

            switch (type) {
                case 'edit':
                    /* Get the row */
                    var row = productCategoriesTable.row(this.rowToProcess);

                    /* Update */
                    row.data(data.row).draw();

                    break;

                case 'delete':
                    /* Get the row to be removed */
                    var row = productCategoriesTable.row(this.rowToProcess);

                    /* Remove the user from the Datatable */
                    row.remove().draw(false);

                    break;

                default:

                    /* Add the new row on the users datatable */
                    productCategoriesTable.row.add(data.row).draw();

                    break;
            }
        },
        resetForm: function () {

            this.rowToProcess = '';

            this.form.errors = [];
            /* Clear the fields except the token. */
            for (var key in this.inputs) {
                if (key != '_token') {
                    this.inputs[key] = '';
                }
            }
        }
    }
});