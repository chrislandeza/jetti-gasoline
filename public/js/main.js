
/**
 * --------------------------------------------------
 *  Helper Functions
 * --------------------------------------------------
 */

/**
 * 
 * @param {type} btn
 * @param {type} text
 * @param {type} status
 * @returns {undefined}
 */
function btnDisabler(btn, text, disable) {
    if (text) {
        $(btn).html(text);
    }

    if (!disable) {
        $(btn).removeAttr('disabled');
    } else {
        $(btn).attr('disabled', 'disabled');

    }
}


/**
 * Add a error-highlight class to an element
 * then remove it after .5 sec
 * 
 * @param {type} element
 */
function error_highlight(element) {
    $(element).addClass('error-highlight').delay(500).queue(function (next) {
        $(this).removeClass('error-highlight');
        next();
    });
}



/**
 * Throw an ajax error notification
 * 
 * @returns {void}
 */
function throwAjaxError() {
    /* Notify the user that there was an error on the request */
    $.notify('An error occured while processing your request. Please reload this page or contact your system administrator.', {
        autoHide: true,
        globalPosition: 'bottom right',
        className: 'error',
        autoHideDelay: 10000,
    });
}




/**
 * --------------------------------------------------
 *  Initializations
 * --------------------------------------------------
 */

/**
 * Initialize datepicker and datetimepicker
 */
function initDateTimePicker(){
  
    
    /* Initialize datetimepicker */
    $('.is-dtpicker').datetimepicker({
        controlType: 'select',
	timeFormat: 'hh:mm tt'
    });
    
    /* Instantiate the Date Selector */
    $("#dateselector").datepicker({
        dateFormat: "yy-mm-dd"
    });

    /* Show the DatePicker when the Calendar Icon is Clicked */
    $('#dateselector').next().on('click', function () {
        $("#dateselector").datepicker('show');
    });
}

/**
 * Initialize tooltip
 */
function initTooltip(){
      /* Initializa tooltip */
    $('[data-toggle="tooltip"]').tooltip();
    
}

/**
 * Initialize all plugin
 */
function initAllPlugin(){
    initTooltip();
    initDateTimePicker();
}

/**
 * Initialize the plugins on document ready
 */
$(function () {
    initAllPlugin();
});






/**
 * Remove the error feedback on input focusout
 */
$(document).on('focusout', 'input', function () {
    $(this).closest('.has-feedback').removeClass('has-error');
    $(this).closest('.has-feedback').find('.help-inline').hide();
});

/**
 * --------------------------------------------------
 * Make the text field only accepts number (with one dot)
 * --------------------------------------------------
 */
$(document).on("keydown", ".numeric-only", function (event) {

    // Allow: backspace, delete, tab, escape, and enter
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
                    (event.keyCode == 65 && event.ctrlKey === true) ||
                    // Allow: home, end, left, right
                            (event.keyCode >= 35 && event.keyCode <= 39) ||
                            (event.keyCode == 190 || event.keyCode == 110)) {

                //allow only one dot
                if (this.value.split('.').length > 1 && (event.keyCode == 190 || event.keyCode == 110)) {
                    /* put a red highlight on the border for 500 miliseconds */
                    error_highlight(this);
                    return false;
                }

                // let it happen, don't do anything
                return;
            }
            else {

                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                    //put a red highlight (500 milisec)
                    error_highlight(this);
                    event.preventDefault();
                }

            }
        });