<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReadingSticksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reading_sticks', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('reading_id')->unsigned();
            $table->foreign('reading_id')->references('id')->on('readings')
            ->onDelete('cascade');

            $table->decimal('stick_opening');
            $table->decimal('stick_closing');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reading_sticks');
    }
}
