<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `jetti-gasoline`
//

// `jetti-gasoline`.`reading_sticks`
$reading_sticks = array(
  array('id' => '1','reading_id' => '1','stick_opening' => '20000.00','stick_closing' => '15000.00','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '2','reading_id' => '2','stick_opening' => '5000.00','stick_closing' => '3000.00','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '3','reading_id' => '3','stick_opening' => '7500.00','stick_closing' => '7000.00','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL)
);
