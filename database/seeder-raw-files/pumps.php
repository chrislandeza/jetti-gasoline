<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `jetti-gasoline`
//

// `jetti-gasoline`.`pumps`
$pumps = array(
  array('id' => '1','pump_number' => 'Pump #1','pump_description' => 'Sample Pump #1 Description','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '2','pump_number' => 'Pump #2','pump_description' => 'Sample Pump #2 Description','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '3','pump_number' => 'Pump #3','pump_description' => 'Sample Pump #3 Description','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL)
);
