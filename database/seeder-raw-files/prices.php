<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `jetti-gasoline`
//

// `jetti-gasoline`.`prices`
$prices = array(
  array('id' => '1','product_id' => '1','amount' => '100.00','effective_datetime' => '2015-07-14 00:00:00','remarks' => 'Initial Price','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '2','product_id' => '2','amount' => '200.00','effective_datetime' => '2015-07-14 00:00:00','remarks' => 'Initial Price','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '3','product_id' => '3','amount' => '300.00','effective_datetime' => '2015-07-14 00:00:00','remarks' => 'Initial Price','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL)
);
