<?php

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{

    public function run()
    {
        if (App::environment() === 'production') {
            exit('I just stopped you getting fired.');
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');


        DB::table('role_user')->truncate();

        DB::table('role_user')->insert([
            ['user_id' => 1, 'role_id' => 1],
            ['user_id' => 2, 'role_id' => 1]
        ]);
    }
}