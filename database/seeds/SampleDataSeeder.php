<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\DB\ProductCategory\ProductCategory;
use App\DB\Product\Product;
use App\DB\Price\Price;
use App\DB\Pump\Pump;
use App\DB\Reading\Reading;
use App\DB\ReadingStick\ReadingStick;
use App\DB\ReadingPump\ReadingPump;
use App\DB\Branch\Branch;

class SampleDataSeeder extends Seeder
{



    public function run()
    {
        if (App::environment() === 'production') {
            exit('I just stopped you getting fired.');
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');


        /**
         * --------------------------------------------------
         *  Sample Branch Data
         * --------------------------------------------------
         */
        Branch::truncate();

        $branches = array(
            array('id' => '1', 'branch_name' => 'Sample Branch #1', 'branch_description' => 'Brief Description for Branch #1',
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL),
            array('id' => '2', 'branch_name' => 'Sample Branch #2', 'branch_description' => 'Brief Description for Branch #2',
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL)
        );

        Branch::insert($branches);



        /**
         * --------------------------------------------------
         *  Product Categories Sample Data
         * --------------------------------------------------
         */
        ProductCategory::truncate();

        $product_categories = array(
            array('id' => '1', 'category_name' => 'Gas', 'category_description' => 'Gas Products',
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'deleted_at' => NULL),
            array('id' => '2', 'category_name' => 'Other', 'category_description' => 'Other Products',
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'deleted_at' => NULL)
        );

        ProductCategory::insert($product_categories);



        /**
         * --------------------------------------------------
         *  Products Sample Data
         * --------------------------------------------------
         */
        Product::truncate();

        $products = array(
            array('id' => '1', 'branch_id' => 1, 'category_id' => '1', 'product_name' => 'Product 1',
                'product_description' => 'Product 1 Description', 'product_quantity' => '100.00',
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'deleted_at' => NULL),
            array('id' => '2', 'branch_id' => 1, 'category_id' => '2', 'product_name' => 'Product 2',
                'product_description' => 'Product 2 Description', 'product_quantity' => '50.00',
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'deleted_at' => NULL),
            array('id' => '3', 'branch_id' => 1, 'category_id' => '1', 'product_name' => 'Product 3',
                'product_description' => 'Product 3 Desciption', 'product_quantity' => '35.00',
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'deleted_at' => NULL)
        );

        Product::insert($products);


        /**
         * --------------------------------------------------
         *  Sample Prices Data
         * --------------------------------------------------
         */
        Price::truncate();

        $prices = array(
            array('id' => '1', 'product_id' => '1', 'amount' => '100.00', 'effective_datetime' => '2015-07-14 00:00:00',
                'remarks' => 'Initial Price', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL),
            array('id' => '2', 'product_id' => '2', 'amount' => '200.00', 'effective_datetime' => '2015-07-14 00:00:00',
                'remarks' => 'Initial Price', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL),
            array('id' => '3', 'product_id' => '3', 'amount' => '300.00', 'effective_datetime' => '2015-07-14 00:00:00',
                'remarks' => 'Initial Price', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL)
        );

        Price::insert($prices);


        /**
         * --------------------------------------------------
         *  Sample Pumps Data
         * --------------------------------------------------
         */
        Pump::truncate();

        $pumps = array(
            array('id' => '1', 'pump_number' => 'Pump #1', 'pump_description' => 'Sample Pump #1 Description',
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'deleted_at' => NULL),
            array('id' => '2', 'pump_number' => 'Pump #2', 'pump_description' => 'Sample Pump #2 Description',
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'deleted_at' => NULL),
            array('id' => '3', 'pump_number' => 'Pump #3', 'pump_description' => 'Sample Pump #3 Description',
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'deleted_at' => NULL)
        );

        Pump::insert($pumps);


        /**
         * --------------------------------------------------
         *  Sample product_pump Data
         * --------------------------------------------------
         */
        DB::table('product_pump')->truncate();

        $product_pump = array(
            array('id' => '1', 'product_id' => '1', 'pump_id' => '1', 'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(), 'deleted_at' => NULL),
            array('id' => '2', 'product_id' => '2', 'pump_id' => '2', 'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(), 'deleted_at' => NULL),
            array('id' => '3', 'product_id' => '3', 'pump_id' => '3', 'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(), 'deleted_at' => NULL),
            array('id' => '4', 'product_id' => '1', 'pump_id' => '2', 'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(), 'deleted_at' => NULL)
        );

        DB::table('product_pump')->insert($product_pump);

        /**
         * --------------------------------------------------
         *  Sample Readings Data
         * --------------------------------------------------
         */
        Reading::truncate();

        $readings = array(
            array('id' => '1', 'product_id' => '1', 'reading_datetime' => '2015-07-15 00:00:00',
                'remarks' => 'Sample Reading for Product 1', 'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(), 'deleted_at' => NULL),
            array('id' => '2', 'product_id' => '2', 'reading_datetime' => '2015-07-15 00:00:00',
                'remarks' => 'Sample Reading for Product 2', 'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(), 'deleted_at' => NULL),
            array('id' => '3', 'product_id' => '3', 'reading_datetime' => '2015-07-16 00:00:00',
                'remarks' => 'Sample Reading for Product 3', 'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(), 'deleted_at' => NULL)
        );

        Reading::insert($readings);

        /**
         * --------------------------------------------------
         *  Sample Stick Reading Data
         * --------------------------------------------------
         */
        ReadingStick::truncate();

        $reading_sticks = array(
            array('id' => '1', 'reading_id' => '1', 'stick_opening' => '20000.00',
                'stick_closing' => '15000.00', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL),
            array('id' => '2', 'reading_id' => '2', 'stick_opening' => '5000.00',
                'stick_closing' => '3000.00', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL),
            array('id' => '3', 'reading_id' => '3', 'stick_opening' => '7500.00',
                'stick_closing' => '7000.00', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL)
        );

        ReadingStick::insert($reading_sticks);


        /**
         * --------------------------------------------------
         *  Sample Pump Reading Data
         * --------------------------------------------------
         */
        ReadingPump::truncate();

        $reading_pumps = array(
            array('id' => '1', 'reading_id' => '1', 'pump_id' => '1', 'pump_opening' => '20000.00',
                'pump_closing' => '25000.00', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL),
            array('id' => '2', 'reading_id' => '2', 'pump_id' => '2', 'pump_opening' => '5000.00',
                'pump_closing' => '7000.00', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL),
            array('id' => '3', 'reading_id' => '3', 'pump_id' => '3', 'pump_opening' => '7500.00',
                'pump_closing' => '8000.00', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                'deleted_at' => NULL)
        );

        ReadingPump::insert($reading_pumps);
    }



}