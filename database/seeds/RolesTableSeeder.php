<?php

use Illuminate\Database\Seeder;
use App\DB\Role\Role;

class RolesTableSeeder extends Seeder
{

    public function run()
    {
        if (App::environment() === 'production') {
            exit('I just stopped you getting fired.');
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');


        Role::truncate();

        Role::create([
            'name' => 'admin',
            'display_name' => 'admin',
            'description' => 'The Admin of the System',
       
        ]);

        Role::create([
            'name' => 'owner',
            'display_name' => 'Owner',
            'description' => 'The Owner of Jetti-Gasoline',

        ]);

       
    }
}