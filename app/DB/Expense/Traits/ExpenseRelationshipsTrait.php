<?php

namespace App\DB\Expense\Traits;

trait ExpenseRelationshipsTrait
{

    /**
     * Get the Branch associated with this Expense
     *
     * @return belongsToMany
     */
    public function branch()
    {
        return $this->belongsTo('App\DB\Branch\Branch', 'branch_id');
    }
}