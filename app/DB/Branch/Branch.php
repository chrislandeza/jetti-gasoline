<?php

namespace App\DB\Branch;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\DB\Branch\Traits\BranchRelationshipsTrait;

class Branch extends Model
{

    use SoftDeletes,
        BranchRelationshipsTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branches';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_name',
        'branch_description',
        'branch_address',
        'contact_number'
    ];

}