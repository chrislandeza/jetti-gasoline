<?php

namespace App\DB\Product\Traits;

trait ProductRelationshipsTrait
{



    /**
     * The Products associated with this Category
     *
     * @return belongsToMany
     */
    public function category()
    {
        return $this->belongsTo('App\DB\ProductCategory\ProductCategory',
                'category_id');
    }



    /**
     * Get the Branch associated with this Product
     *
     * @return belongsToMany
     */
    public function branch()
    {
        return $this->belongsTo('App\DB\Branch\Branch', 'branch_id');
    }



    /**
     * The Pumps associated with this Product
     *
     * @return belongsToMany
     */
    public function pumps()
    {
        return $this->belongsToMany('App\DB\Pump\Pump')->withTimestamps();
    }



    /**
     * The Prices associated with this Product
     * 
     * @return hasMany
     */
    public function prices()
    {
        return $this->hasMany('App\DB\Price\Price', 'product_id', 'id');
    }



    /**
     * The Purchases associated with this Product
     * 
     * @return hasMany
     */
    public function purchases()
    {
        return $this->hasMany('App\DB\Purchase\Purchase', 'product_id', 'id');
    }



    /**
     * The Readings of this Product
     * 
     * @return hasMany
     */
    public function readings()
    {
        return $this->hasMany('App\DB\Reading\Reading', 'product_id', 'id');
    }



}