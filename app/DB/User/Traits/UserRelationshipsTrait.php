<?php

namespace App\DB\User\Traits;

trait UserRelationshipsTrait
{

    /**
     * Get the Branch associated with this User
     *
     * @return belongsToMany
     */
    public function branch()
    {
        return $this->belongsTo('App\DB\Branch\Branch', 'branch_id');
    }

    
}