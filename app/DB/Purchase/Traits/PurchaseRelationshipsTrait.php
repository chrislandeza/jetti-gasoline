<?php

namespace App\DB\Purchase\Traits;

trait PurchaseRelationshipsTrait
{

    /**
     * Get the Product associated with this CPurchase
     *
     * @return belongsToMany
     */
    public function product()
    {
        return $this->belongsTo('App\DB\Product\Product', 'product_id');
    }
}