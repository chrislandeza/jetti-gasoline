<?php

namespace App\DB\ReadingPump\Traits;

trait ReadingPumpRelationshipsTrait
{

    /**
     * The Reading associated with this Pump Reading
     *
     * @return belongsTo
     */
    public function reading()
    {
        return $this->belongsTo('App\DB\Reading\Reading', 'reading_id');
    }

    /**
     * The Pump associated with this PumpReading
     * 
     * @return belongsTo
     */
    public function pump()
    {
        return $this->belongsTo('App\DB\Pump\Pump', 'pump_id');
    }
}