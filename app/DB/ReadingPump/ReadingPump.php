<?php

namespace App\DB\ReadingPump;

use Illuminate\Database\Eloquent\Model;
use App\DB\ReadingPump\Traits\ReadingPumpRelationshipsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReadingPump extends Model
{

    use ReadingPumpRelationshipsTrait,
        SoftDeletes;
}