<?php

namespace App\DB\Price;

use Illuminate\Database\Eloquent\Model;
use App\DB\Price\Traits\PriceRelationshipsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Price extends Model
{

    use PriceRelationshipsTrait,
        SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'prices';

    /**
     * Columns that should be treated as Carbon Instance
     *
     * @var array
     */
    protected $dates = [
        'effective_datetime'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'amount',
        'effective_datetime',
        'remarks'
    ];

    /**
     * Scope for getting the latest price of a product.
     * 
     * @param type $query
     * @return type
     */
    public function scopeLatest($query)
    {
        return $query->orderBy('effective_datetime', 'desc')->first();
    }


    /**
     * Format the effective_datetime before storing
     * 
     * @param type $value
     */
    public function setEffectiveDatetimeAttribute($value)
    {
        $this->attributes['effective_datetime'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }
}