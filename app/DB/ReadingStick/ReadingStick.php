<?php

namespace App\DB\ReadingStick;

use Illuminate\Database\Eloquent\Model;
use App\DB\ReadingStick\Traits\ReadingStickRelationshipsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReadingStick extends Model
{

    use ReadingStickRelationshipsTrait,
        SoftDeletes;
}