<?php

namespace App\DB\ProductCategory\Traits;

trait ProductCategoryRelationshipsTrait
{

    /**
     * Get the Products associated with this Category
     *
     * @return belongsToMany
     */
    public function products()
    {
        return $this->hasMany('App\DB\Product\Product', 'category_id', 'id');
    }
}