<?php
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

/**
 * --------------------------------------------------
 * Implement the default Authentication of Laravel
 * --------------------------------------------------
 */
Route::controller('auth', 'Auth\AuthController');





/**
 * --------------------------------------------------
 * Dashboard Route
 * --------------------------------------------------
 */
Route::get('/',
    ['middleware' => 'auth', 'uses' => 'Dashboard\DashboardController@index']);
Route::get('dashboard',
    ['middleware' => 'auth', 'uses' => 'Dashboard\DashboardController@index']);



/* Route Resource for Readings */
Route::resource('readings', 'Reading\ReadingController');
Route::resource('expenses', 'Expenses\ExpensesController');


/**
 * --------------------------------------------------
 * Inventory Routes
 *
 * @todo - Inject Route Middleware
 * --------------------------------------------------
 */
Route::group([
    'middleware' => ['auth'],
    'prefix' => 'inventory'
    ],
    function() {

    /* The Product Category List */
    Route::get('product-categories', 'ProductCategory\ProductCategoryController@index');


    /* The Product Category List */
    Route::get('products', 'Product\ProductController@index');

    /* The Prices list for specified product */
    Route::get('products/{products}/prices', 'Price\PriceController@index');
    
});


/**
 * --------------------------------------------------
 *  Routes for our Ajax Request
 * --------------------------------------------------
 */
Route::group(['prefix' => 'api'], function()
{

    Route::group([
        'middleware' => ['auth'],
        'prefix' => 'inventory'
        ],
        function() {

        /* Resource Controller for Product Categories API */
        Route::resource('product-categories', 'API\ProductCategory\ProductCategoryController');

        /* Resource Controller for Products API */
        Route::resource('products', 'API\Product\ProductController');

        /* Resource Controller for Product's Prices */
        Route::resource('products/{products}/prices', 'API\Price\PriceController');


    });

    

});






/**
 * --------------------------------------------------
 *  To be removed
 * --------------------------------------------------
 */
// Display all SQL executed in Eloquent
// Display all SQL executed in Eloquent
//Event::listen('illuminate.query', function($query)
//{
//    var_dump($query);
//});
//
