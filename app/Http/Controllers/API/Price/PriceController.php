<?php

namespace App\Http\Controllers\API\Price;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PriceRequest;
use App\DB\Product\Product;
use App\DB\Price\Price;

class PriceController extends Controller
{
    /**
     * App\DB\Product\Product
     * 
     * @var $product
     */
    protected $product;

    /**
     * App\DB\Price
     * 
     * @var $price
     */
    protected $price;

    /**
     * Dependency Injection
     *
     * @todo - Inject Middleware
     * @param App\DB\Product\Product $product
     * @param App\DB\Price\Price $price
     */
    public function __construct(Product $product, Price $price)
    {
        $this->product = $product;

        $this->price = $price;
    }

    /**
     * Show the form for creating a new Price for a Product.
     *
     * @param Request $request
     * @param int $id Product ID
     * @return Response
     */
    public function create(Request $request, $id)
    {
        /* Get the Product */
        $product = $this->product->findOrFail($id);

        /* Get the form */
        $html = view('api.inventory.products.prices.create', compact('product'))->render();

        // Render html from view.
        return response()->json(['success' => true, 'html' => $html]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $id - Product ID
     * @param  App\Http\Requests\PriceRequest  $request
     * @return Response
     */
    public function store($id, PriceRequest $request)
    {

        /* Get the Product */
        $product = $this->product->findOrFail($id);

        /* Create a new price for a product and store it to $price variable */
        $price = $product->prices()->create($request->all());

        /* Generate the Row for our datatable */
        $row = $this->generateRow($price, $product);

        return response()->json(['success' => true, 'row' => $row]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @todo - Route model binding.
     * @param  int  $product_id
     * @param  int  $price_id
     * @return Response
     */
    public function edit($product_id, $price_id)
    {

        /* Get the Product */
        $product = $this->product->findOrFail($product_id);

        /* Get the Price */
        $price = $this->price->findOrFail($price_id);

        /* Get the form */
        $html = view('api.inventory.products.prices.edit',
            compact('product', 'price'))->render();

        // Render html from view.
        return response()->json(['success' => true, 'html' => $html]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @todo - Route model binding.
     * @param  Request  $request
     * @param  int  $product_id - Product ID
     * @param  int  $price_id - Price ID
     * @return Response
     */
    public function update(PriceRequest $request, $product_id, $price_id)
    {
        /**
         * @todo - Check if the price we're trying to update
         * is associated with the current product 
         * or authenticated user's product
         * (to prevent CSRF)
         */
        /* Get the Product */
        $product = $this->product->findOrFail($product_id);

        /* Get the Price @see todo */
        $price = $this->price->findOrFail($price_id);

        /* Update the Resource */
        $price->update($request->all());

        /* Generate the Row */
        $row = $this->generateRow($price, $product);

        /* Respond */
        return response()->json(['success' => true, 'row' => $row]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $product_id  - Product ID
     * @param  $price_id  - Price ID
     * @return Response
     */
    public function destroy($product_id, $price_id)
    {

        $price = $this->price->findOrFail($price_id);

        $price->delete();

        return response()->json(['success' => true]);
    }

    /**
     * Generate the Row (To be used on our DataTable).
     *
     * @param Price $price
     * @param Product $product
     * @param type $row
     */
    protected function generateRow(Price $price, Product $product)
    {

        /* Generate the buttons */
        $actions = view('inventory.products.prices.partials.actions',
            compact('price', 'product'))->render();

        $row = [
            'id' => $price->id,
            'amount' => number_format($price->amount, 2),
            'effective_datetime' => $price->effective_datetime->toDayDateTimeString(),
            'remarks' => $price->remarks,
            'buttons' => $actions
        ];

        return array_flatten($row);
    }
}