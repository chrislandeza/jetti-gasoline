<?php

namespace App\Http\Controllers\Price;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DB\Product\Product;
use App\DB\Price\Price;

class PriceController extends Controller
{
    /**
     * App\DB\Product\Product
     * 
     * @var $product
     */
    protected $product;

    /**
     * App\DB\Price
     * 
     * @var $price
     */
    protected $price;

    /**
     * Dependency Injection
     *
     * @todo - Inject Middleware
     * @param Product $product
     * @param Price $price
     */
    public function __construct(Product $product, Price $price)
    {
        $this->product = $product;

        $this->price = $price;
    }

    /**
     * Show the prices of the given product id.
     *
     * @param type $id
     */
    public function index($id)
    {

        /* Get the product */
        $product = $this->product->findOrFail($id);

        /* Get the latest price details */
        $price = $product->prices()->latest();
       
        return  view('inventory.products.prices.index', compact('product', 'price'));

    }
}