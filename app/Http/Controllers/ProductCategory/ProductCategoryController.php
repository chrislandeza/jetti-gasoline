<?php

namespace App\Http\Controllers\ProductCategory;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DB\ProductCategory\ProductCategory;

class ProductCategoryController extends Controller
{
    /* @var ProductCategory */
    protected $productCategory;

    /**
     * Dependency Injection
     * 
     * @todo - Inject Middleware
     * @param ProductCategory $productCategory
     */
    public function __construct(ProductCategory $productCategory)
    {
        $this->productCategory = $productCategory;
    }

    /**
     * Display a listing of the Product Categories.
     *
     * @return Response
     */
    public function index()
    {
        $productCategories = $this->productCategory->all();

        return view('inventory.product-categories.index', compact('productCategories'));
    }
}