<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;

class PriceRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        /**
         * @todo - Check if the user is authorized to perform this request
         * 
         * return $this->auth->user()->can('add_user|edit_user');
         */
        return true;
    }

    /**
     * Modify some input before validation
     *
     * @return $attributes
     */
    public function all()
    {
        $attributes = parent::all();


        /**
         * Check if the date is parsable and format it to the default DateTime format
         *
         * The validator will try to find the duplicate for effective_datetime and it
         * will passed becuase our input (string) format is not the same as the
         * default database DateTime format, as a result, the app will throw
         * an error: SQLSTATE[23000].
         */
        if (dateIsParsable($attributes['effective_datetime'])) {
            $attributes['effective_datetime'] = Carbon::parse($attributes['effective_datetime'])->format('Y-m-d H:i:s');
        }

        return $attributes;
    }

    /**
     * Custom attributes for our columns
     * 
     * @return array
     */
    public function attributes()
    {
        return [
            'effective_datetime' => 'Effective Date/Time'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'POST': {

                    return [
                        'amount' => 'required|numeric',
                        'effective_datetime' => 'required|date|unique:prices,effective_datetime,NULL,id,product_id,'.$this->route('products')
                    ];
                }
            case 'PUT':
            case 'PATCH': {
                    return [
                        'amount' => 'required|numeric',
                        'effective_datetime' => 'required|date|unique:prices,effective_datetime,'.$this->route('prices').',id,product_id,'.$this->route('products')
                    ];
                }
            default:break;
        }
    }
}