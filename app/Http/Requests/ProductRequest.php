<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        /**
         * @todo - Check if the user is authorized to perform this request
         * 
         * return $this->auth->user()->can('add_user|edit_user');
         */
        return true;
    }

    /**
     * Custom Validation Messages
     *
     * @return type
     */
    public function messages()
    {
        return [
            'category_id.required' => 'Category is required'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'POST': {
                    return [
                        'product_name' => 'required',
                        'product_quantity' => 'required|numeric',
                        'category_id' => 'required'
                    ];
                }
            case 'PUT':
            case 'PATCH': {
                    return [
                        'product_name' => 'required',
                        'category_id' => 'required'
                    ];
                }
            default:break;
        }
    }
}