<?php

/**
 * --------------------------------------------------
 * Custom helper Functions for our application
 * We used psr-4 to Autoload this file
 * See composer.json's autoload block.
 * --------------------------------------------------
 */
use Carbon\Carbon;

/**
 * --------------------------------------------------
 * Toggles.
 * --------------------------------------------------
 */
if (!function_exists('set_active')) {

    /**
     * Put a custom CSS class when the condition is true
     *
     * @param type $path
     * @param type $active
     * @return str
     */
    function set_active($path, $active = 'active')
    {

        return call_user_func_array('Request::is', (array) $path) ? $active : '';
    }
}


if (!function_exists('isParsable')) {

    /**
     * Check if the given date is parsable
     *
     * @param str $string
     * @return boolean
     */
    function dateIsParsable($string)
    {

        $date = date_parse($string);
        if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"])) {
            return true;
        }
        return false;
    }
}


if (!function_exists('withEmpty')) {


    /**
     * Create a list for Form::select
     * with optional empty value.
     *
     *
     * @param type $selectList
     * @param type $emptyLabel
     * @return array
     */
    function withEmpty($selectList, $emptyLabel = '')
    {
        return array('' => $emptyLabel) + $selectList;
    }
}
