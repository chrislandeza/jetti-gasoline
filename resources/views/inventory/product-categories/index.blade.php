@extends('layouts.master')

@section('page-title', 'Inventory | Jetti Gasoline')

@section('master-styles')
<link href="{{ asset('vendor/dataTables/DT_bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')

<div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
        <h1>Product Categories</h1>
    </div>
    <div class="pull-right">
        <ol class="breadcrumb">
            <li class="active">Product Categories</li>
        </ol>
    </div>
</div>


<div class="container clear_both padding_fix" id="app">

    <!-- Datatable Start -->
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <div class="header">
                    <div class="actions">
                        <div class="btn-group">
                            <button
                                data-target="{{ action('API\ProductCategory\ProductCategoryController@create') }}"
                                class="btn btn-default"
                                type="button"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Add New"
                                v-on="click: create('New Category', $event)"
                                v-attr="disabled: processingRequest"
                                >
                                <i class="fa fa-plus-circle"></i>
                            </button>
                        </div>
                    </div>
                    <h3 class="content-header">Categories <span class="badge"></span></h3>
                </div>
                <div class="porlets-content">
                    <div class="table-responsive">
                        <table  class="display table table-bordered table-striped table-responsive" id="app-datatable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th></th>

                                </tr>
                            </thead>
                            <tbody>

                                @include('inventory.product-categories.partials.list')

                            </tbody>

                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th></th>
                                </tr>
                            </tfoot>


                        </table>
                    </div><!--/table-responsive-->
                </div><!--/porlets-content-->
            </div><!--/block-web-->
        </div><!--/col-md-12-->
    </div><!--/row-->

    <!-- Modal -->
    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">@{{ form.title }}</h4>
                </div>

                <div id="form-content"></div>

            </div>
        </div>
    </div>





</div>



@stop


@section('master-scripts')
<script src="{{ asset('vendor/dataTables/dataTables.js') }}"></script>
<script src="{{ asset('vendor/dataTables/DT_bootstrap.js') }}"></script>
<script src="{{ asset('js/src/crud.js') }}"></script>
@stop