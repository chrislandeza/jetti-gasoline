@foreach ($productCategories as $productCategory)


<tr>
    <td>{{ $productCategory->id }}</td>

    <td>{{ $productCategory->category_name }}</td>
    <td>{{ $productCategory->category_description }}</td>

    <td>
        @include('inventory.product-categories.partials.actions', $productCategory)
    </td>
</tr>



@endforeach