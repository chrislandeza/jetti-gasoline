@foreach ($product->prices as $price)

<tr>
    <td>{{ $price->id }}</td>
    <td>{{ $price->amount }}</td>
    <td>{{ $price->effective_datetime->toDayDateTimeString()  }}</td>
    <td>{{ $price->remarks }}</td>
    <td>
        @include('inventory.products.prices.partials.actions')
    </td>

</tr>

@endforeach