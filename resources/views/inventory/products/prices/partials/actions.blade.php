<div class='pull-right'>
    <div class="btn-group" role="group">

        <button
            type="button"
            class="btn btn-default btn-sm btn-products-edit"
            data-target="{{ action('API\Price\PriceController@edit', [$product->id, $price->id]) }}"
            v-on="click: edit('Edit Price', $event)"
            v-attr="disabled: processingRequest"
        >
                <i class="fa fa-edit"></i> Edit
        </button>

        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle" type="button">
                <span class="caret"></span>&nbsp;
            </button>

            <ul role="menu" class="dropdown-menu pull-right">
                <li>
                    <a  href="#"
                        data-target="{{ action('API\Price\PriceController@destroy', [$product->id, $price->id]) }}"
                        v-on="click: destroy"
                    >
                        <i class="fa fa-trash-o"></i> Delete
                    </a>
                </li>
            </ul>

        </div>
    </div>
</div>