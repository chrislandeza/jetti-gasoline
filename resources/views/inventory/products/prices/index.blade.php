@extends('layouts.master')

@section('page-title', 'Inventory | Jetti Gasoline')

@section('master-styles')
<link href="{{ asset('vendor/dataTables/DT_bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')

<div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
        <h1>{{ $product->product_name }} </h1>
    </div>
    <div class="pull-right">
        <ol class="breadcrumb">
            <li><a href="{{ url('inventory/products') }}">Products</a></li>
            <li class="active">Manage {{ $product->product_name }} Price</li>
        </ol>
    </div>
</div>


<div class="container clear_both padding_fix" id="app">

    
    <!-- Datatable Start -->
    <div class="row">
        <div class="col-md-8">
            <div class="block-web">
                <div class="header">
                    <div class="actions">
                        <div class="btn-group">

                            <button 
                                data-target="{{ action('API\Price\PriceController@create', $product->id) }}"
                                class="btn btn-default"
                                type="button"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Add New"
                                v-on="click: create('{{ $product->product_name }} New Price', $event)"
                                v-attr="disabled: processingRequest"
                                >
                                <i class="fa fa-plus-circle"></i>
                            </button>
                        </div>
                    </div>
                    <h3 class="content-header">Price History <span class="badge"></span></h3>
                </div>
                <div class="porlets-content">
                    <div class="table-responsive">
                        <table  class="display table table-bordered table-striped table-responsive" id="app-datatable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Amount</th>
                                    <th>Date Effective</th>
                                    <th>Remarks</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @include('inventory.products.prices.partials.list')
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Amount</th>
                                    <th>Date Effective</th>
                                    <th>Remarks</th>
                                    <th></th>
                                </tr>
                            </tfoot>


                        </table>
                    </div><!--/table-responsive-->
                </div><!--/porlets-content-->
            </div><!--/block-web-->
        </div><!--/col-md-8-->

        <div class='col-md-4'>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Product Details</h3>
                </div>
                <div class="panel-body">
                    <dl class="">
                        <dt>Name:</dt>
                        <dd>{{ $product->product_name }}</dd>

                        <dt>Category:</dt>
                        <dd>{{ $product->category->category_name }}</dd>

                        <dt>Description:</dt>
                        <dd>{{ $product->product_description }}</dd>

                        <dt>Latest Effective Price:</dt>
                        <dd>
                            {{ $price->amount or 'No price set' }}


                            @if ($price->count())
                                <span class="help-block">
                                    Date Effective:
                                    {{ $price->effective_datetime->diffForHumans() }}
                                </span>
                            @endif
                        </dd>


                    </dl>
                </div>
            </div>

        </div>
    </div><!--/row-->


    <!-- Modal -->
    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">@{{ form.title }}</h4>
                </div>
                <div id="form-content"></div>
            </div>
        </div>
    </div>

</div>



@stop


@section('master-scripts')
<script src="{{ asset('vendor/dataTables/dataTables.js') }}"></script>
<script src="{{ asset('vendor/dataTables/DT_bootstrap.js') }}"></script>
<script src="{{ asset('js/src/crud.js') }}"></script>
@stop