
<div class="modal-body">

    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <span class="theme_color">*</span> Indicates required field.

    </div>


     <div class="alert alert-danger alert-dismissible" v-show="errors" role="alert">
        <strong>Please correct the errors in the form.</strong>
        <ul class="">
            <li v-repeat="error: form.errors">@{{ error }}</li>
        </ul>
    </div>



    <div class="form-group row has-feedback" v-class="has-error: form.errors.product_name">
        <label class="col-sm-3 control-label">Product Name <span class="theme_color">*</span></label>
        <div class="col-sm-9">
            {!! Form::text('product_name', null,
                [
                    'id' => 'product_name',
                    'class'=>'form-control',
                    'v-model' => 'inputs.product_name'
                ]
            ) !!}
            <span class="help-inline text-danger" v-show="form.errors.product_name">@{{ form.errors.product_name }}</span>
        </div>
    </div><!--/form-group-->


    
    <div class="form-group" v-class="has-error: form.errors.category_id">
        <label class="col-sm-3 control-label">Category <span class="theme_color">*</span></label>
        <div class="col-sm-9">
            {!! Form::select('category_id', $categories, $product->category_id,
                [
                    'id' => 'category_id',
                    'class'=>'form-control',
                    'v-model' => 'inputs.category_id'
                ]
            ) !!}
            <span class="help-inline text-danger" v-show="form.errors.category_id">
                @{{ form.errors.category_id }}
            </span>
        </div>
    </div><!--/form-group-->



    <div class="form-group row has-feedback" v-class="has-error: form.errors.product_description">
        <label class="col-sm-3 control-label">Description</label>
        <div class="col-sm-9">
            {!! Form::text('product_description', null,
                [
                    'id' => 'product_description',
                    'class'=>'form-control',
                    'v-model' => 'inputs.product_description'
                ]
            ) !!}
            <span class="help-inline text-danger" v-show="form.errors.product_description">@{{ form.errors.product_description }}</span>
        </div>
    </div><!--/form-group-->

    @if (!isset($is_edit))
        <div class="form-group row has-feedback" v-class="has-error: form.errors.product_quantity">
            <label class="col-sm-3 control-label">Initial Quantity <span class="theme_color">*</span></label>
            <div class="col-sm-9">
                {!! Form::text('product_quantity', null,
                    [
                        'id' => 'product_quantity',
                        'class'=>'form-control numeric-only',
                        'v-model' => 'inputs.product_quantity'
                    ]
                ) !!}
                <span class="help-inline text-danger" v-show="form.errors.product_quantity">@{{ form.errors.product_quantity }}</span>
            </div>
        </div><!--/form-group-->
    @endif
    

    
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary" v-attr="disabled: processingRequest">{{ $submitBtnText }}</button>
</div>



