
<div class="modal-body">

    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <span class="theme_color">*</span> Indicates required field.

    </div>


    <div class="alert alert-danger alert-dismissible" v-show="errors" role="alert">
        <strong>Please correct the errors in the form.</strong>
        <ul class="">
            <li v-repeat="error: form.errors">@{{ error }}</li>
        </ul>
    </div>


    <div class='row'>

        <div class='col-sm-4'>
            <div class="form-group has-feedback" v-class="has-error: form.errors.amount">
                <label>Amount: <span class="theme_color">*</span></label>
                {!! Form::text('amount', null,
                    [
                        'id' => 'amount',
                        'class'=>'form-control numeric-only',
                        'placeholder' => '0.00',
                        'v-model' => 'inputs.amount',
                        'number' => 'number'
                    ]
                ) !!}

            </div>
        </div>

        <div class='col-sm-4'>
            <div class="form-group has-feedback" v-class="has-error: form.errors.effective_datetime">
                <label>Date Effective: <span class="theme_color">*</span> </label>
                {!! Form::text('effective_datetime', null,
                    [
                        'id' => 'effective_datetime',
                        'class'=>'form-control is-dtpicker',
                        'placeholder' => 'Date Effective',
                        'v-model' => 'inputs.effective_datetime',
                        'v-datetimepicker' => 'inputs.effective_datetime'
                    ]
                ) !!}

            </div>
        </div>
        
        <div class='col-sm-4'>
            <div class="form-group has-feedback" v-class="has-error: form.errors.remarks">
                <label>Remarks: </label>
                {!! Form::text('remarks', null,
                    [
                        'id' => 'remarks',
                        'class'=>'form-control',
                        'placeholder' => 'Remarks',
                        'v-model' => 'inputs.remarks'
                    ]
                ) !!}

            </div>
        </div>


    </div>

    <pre>@{{ $data | json }}</pre>


</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary" v-attr="disabled: processingRequest">{{ $submitBtnText }}</button>
</div>






