{!! Form::open(
    [
        'action' => ['API\Price\PriceController@store', $product->id],
        'id' => 'form-price',
        'v-on' => 'submit: store'
    ]
) !!}

    @include('api.inventory.products.prices.partials.form', ['submitBtnText' => 'Add'])

{!! Form::close() !!}