{!! Form::model(
    $price,
        [
            'method' => 'PATCH',
            'action' => ['API\Price\PriceController@update', $product->id, $price->id],
            'id' => 'form-products',
            'v-on' => 'submit: update'
        ]
) !!}

    @include('api.inventory.products.prices.partials.form', ['submitBtnText' => 'Save','is_edit' => true])

{!! Form::close() !!}

