{!! Form::model(
    $product,
        [
            'method' => 'PATCH',
            'action' => ['API\Product\ProductController@update', $product->id], 
            'id' => 'form-products',
            'class' => 'form-horizontal',
            'v-on' => 'submit: update'
        ]
) !!}

    @include('api.inventory.products.partials.form', ['submitBtnText' => 'Save','is_edit' => true])

{!! Form::close() !!}

