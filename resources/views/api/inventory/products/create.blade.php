{!! Form::open(
    [
        'action' => 'API\Product\ProductController@store',
        'id' => 'form-products',
        'class' => 'form-horizontal',
        'v-on' => 'submit: store'
    ]
) !!}

    @include('api.inventory.products.partials.form', ['submitBtnText' => 'Add'])

{!! Form::close() !!}