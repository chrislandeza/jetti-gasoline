<div class='pull-right'>
    <div class="btn-group" role="group">

        <a type="button" class="btn btn-default btn-sm btn-product-categories-edit" href="{{ action('API\ProductCategory\ProductCategoryController@edit', $productCategory->id) }}" ><i class="fa fa-edit"></i> Edit</a>

        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle" type="button">
                <span class="caret"></span>&nbsp;
            </button>

            <ul role="menu" class="dropdown-menu pull-right">
                <li><a href="{{ action('API\ProductCategory\ProductCategoryController@destroy', $productCategory->id) }}" class="btn-product-categories-delete"><i class="fa fa-trash-o"></i> Delete</a></li>
            </ul>

        </div>
    </div>
</div>