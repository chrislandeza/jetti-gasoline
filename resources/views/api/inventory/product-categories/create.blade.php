{!! Form::open(
    [
        'action' => 'API\ProductCategory\ProductCategoryController@store',
        'id' => 'form-product-categories',
        'class' => 'form-horizontal',
        'v-on' => 'submit: store'
    ]
) !!}

    @include(
        'api.inventory.product-categories.partials.form',
        ['submitBtnText' => 'Add']
    )

{!! Form::close() !!}