<!-- Default Scripts needed for our App. -->
<script src="{{ asset('vendor/jquery/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('vendor/vue/vue.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('vendor/jquery-ui/timepicker.min.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('lib/bootbox.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/common-script.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/jPushMenu.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/side-chats.js') }}"></script>
<script src="{{ asset('lib/notify.js') }}"></script>
<script>
paceOptions = {
    // Configuration goes here. Example:
    elements: true,
    restartOnPushState: true,
    restartOnRequestAfter: true,
    ajax: {
        trackMethods: ['GET', 'POST', 'DELETE', 'PUT', 'PATCH']
    }
}
</script>
<script src="{{ asset('lib/pace.min.js') }}"></script>

